package coordinates;

import coordinates.Vector;
import numbers.RoundedDouble;

public class PolarVector implements Vector<PolarVector> {

	protected double theta; //Angle of the vector
	protected double radius; //Radial Distance
	
	protected PolarVector(){ }
	
	public PolarVector(double theta, double radius) {
		setTheta(theta);
		setRadius(radius);
	}
	
	public PolarVector(CartesianVector v){
		double angle = RoundedDouble.round(Math.atan(v.getY() / v.getX()), 6);
		double radius = RoundedDouble.round(v.getMagnitude(), 6);
		
		setTheta(angle);
		setRadius(radius);
	}
	
	@Override
	public PolarVector add(PolarVector v) {
		CartesianVector V1 = this.toCartesian();
		CartesianVector V2 = v.toCartesian();
		
		CartesianVector sum = V1.add(V2);
		
		return new PolarVector(sum);
	}
	
	public CartesianVector toCartesian() {
		double x = RoundedDouble.round(radius * Math.cos(theta), 6);
		double y = RoundedDouble.round(radius * Math.sin(theta), 6);
		
		return new CartesianVector(x,y);
	}
	
	@Override
	public PolarVector scale(double n) {
		return new PolarVector(theta, radius * n);
	}
	
	@Override
	public PolarVector subtract(PolarVector v2){
		CartesianVector V1 = this.toCartesian();
		CartesianVector V2 = v2.toCartesian();
		
		CartesianVector diff = V1.subtract(V2);
		
		return new PolarVector(diff);
	}
	
	public double getMagnitude() {
		return radius;
	}
	
	public double getTheta() {
		return this.theta;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	protected void setTheta(double theta) {
		this.theta = theta;
	}
	
	protected void setRadius(double radius) {
		this.radius = radius;
	}	

}
