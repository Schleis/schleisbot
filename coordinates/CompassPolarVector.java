package coordinates;

import numbers.RoundedDouble;

public class CompassPolarVector extends PolarVector {

	public CompassPolarVector(CartesianVector v) {
		double angle = findAngle(v.getX(), v.getY());
		double radius = v.getMagnitude();
		
		setTheta(angle);
		setRadius(radius);
	}
	
	public CompassPolarVector(double theta, double radius) {
		super(theta, radius);
	}

	public CartesianVector toCartesian() {
		double x = RoundedDouble.round(radius * Math.sin(theta), 6);
		double y = RoundedDouble.round(radius * Math.cos(theta), 6);
		
		return new CartesianVector(x,y);
	}
	
	private double findAngle(double x, double y) {
		//Angle in Q1
		if (0 <= x && 0 <= y) {
			return Math.atan(x / y);
		} else if (0 <= 0 && 0 > y) {
			//Angle in Q2
			return Math.atan(x / y) + Math.PI;
		} else {
			return Math.atan(x/y) + 2 * Math.PI;
		}
	}
}
