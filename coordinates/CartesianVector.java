package coordinates;

public class CartesianVector implements Vector<CartesianVector> {
	private double x;
	private double y;
	
	public CartesianVector(double x, double y) {
		setX(x);
		setY(y);
	}
	
	public double getMagnitude() {
		return Math.sqrt(x*x + y*y);
	}
	
	@Override
	public CartesianVector add(CartesianVector v2){
		double newX = this.getX() + v2.getX();
		double newY = this.getY() + v2.getY();
		
		return new CartesianVector(newX, newY);
	}
	
	@Override
	public CartesianVector scale(double n) {
		double newX = x * n;
		double newY = y * n;
		
		return new CartesianVector(newX, newY);
	}
	
	@Override
	public CartesianVector subtract(CartesianVector v2) {
		double newX = this.getX() - v2.getX();
		double newY = this.getY() - v2.getY();
		
		return new CartesianVector(newX, newY);
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	private void setX(double x) {
		this.x = x;
	}
	
	private void setY(double y) {
		this.y = y;
	}
}
