package coordinates;

public interface Vector<t extends Vector<t>> {

	double getMagnitude();
	t add(t v2);
	t scale (double n);
	t subtract(t v2);
}
