package botTests;

import static org.junit.Assert.*;

import org.junit.Test;
import coordinates.CompassPolarVector;
import coordinates.CartesianVector;

public class CompassPolarVectorTest {

	@Test
	public void testToCartesian() {
		CompassPolarVector testVector = new CompassPolarVector(Math.PI/2, 1);
		CartesianVector resultVector = testVector.toCartesian();
		
		assertEquals(1, resultVector.getX(), 0.001);
		assertEquals(0, resultVector.getY(), 0.001);
		
		CompassPolarVector testVector1 = new CompassPolarVector(1.6164309, 85.2210958);
		CartesianVector resultVector1 = testVector1.toCartesian();
		
		assertEquals(85.132, resultVector1.getX(), 0.001);
		assertEquals(21.5, resultVector1.getY(), 0.001);
	}

	@Test
	public void testCompassPolarVectorCartesianVector() {
		CartesianVector testVector = new CartesianVector(1,0);
		CompassPolarVector resultVector = new CompassPolarVector(testVector);
		
		assertEquals(Math.PI/2, resultVector.getTheta(), 0.001);
		assertEquals(1, resultVector.getRadius(), 0.001);
		assertEquals(1, resultVector.getMagnitude(), 0.001);
		
		CartesianVector testVector1 = new CartesianVector(1, 1);
		CompassPolarVector resultVector1 = new CompassPolarVector(testVector1);
		
		assertEquals(Math.PI/4, resultVector1.getTheta(), 0.0001);
		assertEquals(Math.sqrt(2), resultVector1.getRadius(), 0.0001);
		
		CartesianVector testVector2 = new CartesianVector(1, -1);
		CompassPolarVector resultVector2 = new CompassPolarVector(testVector2);
		
		assertEquals((3 * Math.PI)/4, resultVector2.getTheta(), 0.0001);
		assertEquals(Math.sqrt(2), resultVector2.getMagnitude(), 0.0001);
		assertEquals(Math.sqrt(2), resultVector2.getRadius(), 0.0001);
		
		CartesianVector testVector3 = new CartesianVector(-1, -1);
		CompassPolarVector resultVector3 = new CompassPolarVector(testVector3);
		
		assertEquals((5 * Math.PI)/4, resultVector3.getTheta(), 0.0001);
		
		CartesianVector testVector4 = new CartesianVector(-1, 1);
		CompassPolarVector resultVector4 = new CompassPolarVector(testVector4);
		
		assertEquals((7* Math.PI)/4, resultVector4.getTheta(), 0.0001);
	}

}
