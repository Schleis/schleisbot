package botTests;

import static org.junit.Assert.*;

import org.junit.Test;

import coordinates.*;

public class PolarVectorTests {

	@Test
	public void testPolarVector() {
		PolarVector Vr = new PolarVector(Math.PI, 2.3);
		assertTrue(Vr instanceof Vector);
		
		assertEquals("Angle is not correct", Math.PI, Vr.getTheta(), 0);
		assertEquals("Radius is not correct", 2.3, Vr.getRadius(), 0);
		assertEquals("Get magnitude", 2.3, Vr.getMagnitude(), 0);
	}

	@Test
	public void testAdd() {
		fail("Not done yet");
	}
	
	@Test
	public void testToCartesian() {
		PolarVector p = new PolarVector(Math.PI/2, 1);
		CartesianVector c = new CartesianVector(0, 1);
		assertEquals("Not correct X Coordinates", c.getX(), p.toCartesian().getX(), 0.1);
		assertEquals("Not correct Y Coordinates", c.getY(), p.toCartesian().getY(), 0.1);
	}
	
	@Test
	public void testPolarVectorCartesianVector() {
		CartesianVector testVector = new CartesianVector(0,1);
		PolarVector resultVector = new PolarVector(testVector);
		
		assertEquals(Math.PI/2, resultVector.getTheta(), 0.001);
		assertEquals(1, resultVector.getRadius(), 0.001);
		assertEquals(1, resultVector.getMagnitude(), 0.001);
		
		CartesianVector testVector1 = new CartesianVector(1, 1);
		PolarVector resultVector1 = new PolarVector(testVector1);
		
		assertEquals(Math.PI/4, resultVector1.getTheta(), 0.0001);
		assertEquals(Math.sqrt(2), resultVector1.getRadius(), 0.0001);
		
		CartesianVector testVector2 = new CartesianVector(1, -1);
		PolarVector resultVector2 = new PolarVector(testVector2);
		
		assertEquals((3 * Math.PI)/4, resultVector2.getTheta(), 0.0001);
		assertEquals(50, resultVector2.getMagnitude(), 0.0001);
		assertEquals(50, resultVector2.getRadius(), 0.0001);
		
		CartesianVector testVector3 = new CartesianVector(-1, -1);
		PolarVector resultVector3 = new PolarVector(testVector3);
		
		assertEquals((5 * Math.PI)/4, resultVector3.getTheta(), 0.0001);
		
		CartesianVector testVector4 = new CartesianVector(-1, 1);
		PolarVector resultVector4 = new PolarVector(testVector4);
		
		assertEquals((7* Math.PI)/4, resultVector4.getTheta(), 0.0001);
	}

}
