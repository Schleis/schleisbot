package botTests;

import org.junit.Test;
import static org.junit.Assert.*;
import numbers.RoundedDouble;

public class RoundedDoubleTest {

	@Test
	public void TestRound() {
		double result = RoundedDouble.round(Math.PI, 4);
		assertEquals(3.1416, result, 0.00005);
		
		//Get a value that should be zero but Java will make not quite
		result = RoundedDouble.round(Math.sin(Math.PI), 5);
		assertEquals(0, result, 0.000005);
		
		result = RoundedDouble.round(Math.E, 2);
		assertEquals(2.72, result, 0.005);
	}
}
