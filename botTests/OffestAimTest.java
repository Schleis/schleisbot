package botTests;

import static org.junit.Assert.*;

import org.junit.Test;

import aiming.offsetAim;
import schleisBot.*;

public class OffestAimTest {
	
	private class MockEnemyBot extends EnemyBot {
		MockEnemyBot (double heading, double velocity, double bearing, double distance) {
			this.bearing = bearing;
			this.heading = heading;
			this.velocity = velocity;
			this.distance = distance;
		}
	}
	
	@Test
	public void testOffsetAimMovingAway() {
		MockEnemyBot botMock = new MockEnemyBot(Math.PI/2, 8.0, Math.PI/2, 32.0);
		offsetAim aiming = new offsetAim(botMock);
		
		assertEquals(Math.PI/2, aiming.getGunHeading(1), 0.001);
	}
	
	@Test
	public void testOffsetAimMovingCloser() {
		MockEnemyBot botMock = new MockEnemyBot((3 * Math.PI)/2, 8.0, Math.PI/2, 32.0);
		offsetAim aiming = new offsetAim(botMock);
		
		assertEquals(Math.PI/2, aiming.getGunHeading(1), 0.001);
	}
	
	@Test
	public void testOffsetAimMovingPerpindicular() {
		MockEnemyBot botMock = new MockEnemyBot(Math.PI, 13.6, Math.PI/2, 30);
		offsetAim aiming = new offsetAim(botMock);
		assertEquals(143 * Math.PI / 180, aiming.getGunHeading(1), 0.05);
	}
	
	@Test
	public void testOffsetAimNotMoving() {
		MockEnemyBot botMock = new MockEnemyBot(2.5, 0, 1.15, 34);
		offsetAim aiming = new offsetAim(botMock);
		assertEquals(1.15, aiming.getGunHeading(1), 0.001);
	}

}
