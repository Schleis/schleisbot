package botTests;

import static org.junit.Assert.*;

import org.junit.Test;

import schleisBot.EnemyBot;

import robocode.ScannedRobotEvent;

public class EnemyBotTests {

	@Test
	public void testEnemyBotScannedRobotEventDouble() {
		ScannedRobotEvent scannedEvent = new ScannedRobotEvent("MockBot", 100, Math.PI / 2, 65, Math.PI, 8);
		EnemyBot enemy = new EnemyBot();
		enemy.updateEnemy(scannedEvent, Math.PI/4);
		
		assertEquals(65, enemy.getDistance(), 0.001);
		assertEquals((3 * Math.PI)/4, enemy.getBearing(), 0.001);
		assertEquals(Math.PI, enemy.getHeading(), 0.001);
		
		ScannedRobotEvent scannedEvent1 = new ScannedRobotEvent("MockBot", 100, -Math.PI / 2, 65, Math.PI, 8);
		EnemyBot enemy1 = new EnemyBot();
		enemy.updateEnemy(scannedEvent1, Math.PI/4);
		
		assertEquals(65, enemy1.getDistance(), 0.001);
		assertEquals((7 * Math.PI)/4, enemy1.getBearing(), 0.001);
		assertEquals(Math.PI, enemy1.getHeading(), 0.001);
	}

}
