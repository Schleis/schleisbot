package botTests;

import static org.junit.Assert.*;

import org.junit.Test;

import robocode.Rules;

import movement.*;

public class BasicMovementTest {

	@Test
	public void testGetTurn() {
		Basic movement = new Basic();
		
		double turn = movement.getTurn(Math.PI / 4, Math.PI / 2);
		
		assertEquals(- Rules.MAX_TURN_RATE_RADIANS, turn, 0.001);
		
		turn = movement.getTurn(3 * Math.PI / 4, Math.PI/2);
		assertEquals(- Rules.MAX_TURN_RATE_RADIANS, turn, 0.001);
	}

}
