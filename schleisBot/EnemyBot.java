package schleisBot;

import robocode.ScannedRobotEvent;

public class EnemyBot {
	protected double bearing;
	protected double heading;
	protected double velocity;
	protected double health;
	protected double expectedHealth = 0.00;
	protected double distance;

	public EnemyBot() {
		expectedHealth = 100.0;
	}
	
	public void updateEnemy(ScannedRobotEvent e, double bodyHeading) {
		setEnemyBearing(e.getBearingRadians(), bodyHeading);
		heading = e.getHeadingRadians();
		velocity = e.getVelocity();
		health = e.getEnergy();
		distance = e.getDistance();
	}
	
	public double getHeading() {
		return heading;
	}
	
	public double getVelocity() {
		return velocity;
	}
	
	public double getBearing() {
		return bearing;
	}
	
	public double getDistance() {
		return distance;
	}
	
	private void setEnemyBearing(double enemyBearing, double bodyHeading) {
		if(-enemyBearing > bodyHeading){
			bearing = bodyHeading + enemyBearing + (2 * Math.PI);
		} else {
			bearing = bodyHeading + enemyBearing;
		}
	}
}
