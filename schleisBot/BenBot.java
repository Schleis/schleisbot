package schleisBot;

import aiming.AimStrategy;
import aiming.offsetAim;
import robocode.*;
import robocode.util.*;
import schleisBot.EnemyBot;
import coordinates.*;
import movement.*;

public class BenBot extends AdvancedRobot {

	double angleToEnemy;
	EnemyBot enemy = null;
	boolean lock;
	AimStrategy aiming;
	CartesianVector myLocation;
	Movement moving = null;
	
	public void run() {
		
		this.setAdjustGunForRobotTurn(true);
		this.setAdjustRadarForGunTurn(true);
		this.setAdjustRadarForRobotTurn(true);
		
		this.enemy = new EnemyBot();
		
		do {
			myLocation = new CartesianVector(getX(), getY());
			lock = false;
			if (getRadarTurnRemaining() == 0.0) {
				setTurnRadarRightRadians( Double.POSITIVE_INFINITY);
			}
			
			if(null != moving) {
				setTurnRight(moving.getTurn(getHeadingRadians(), enemy.getBearing()));
			}
			
			aimGun();
			
			execute();
			
			if(lock) {
				out.println("Locked");
			} else {
				out.println("No lock");
			}
			
		} while (true);
	}
	
	public void onScannedRobot(ScannedRobotEvent e) {
		lock = true;
		enemy.updateEnemy(e, getHeadingRadians());
		
		out.println("Enemy Bearing " + e.getBearingRadians());
		aiming = new offsetAim(enemy);
		double radarTurn = getHeadingRadians() + e.getBearingRadians() - getRadarHeadingRadians();
		
		setTurnRadarRightRadians(Utils.normalRelativeAngle(radarTurn * 2));
		
		moving = new Basic();
		
	}
	
	private void aimGun() {
		if (null == aiming) {
			setTurnGunRightRadians(Rules.GUN_TURN_RATE_RADIANS);
		} else {
			double aimHeading = aiming.getGunHeading(3);
			double gunTurn = aiming.getGunTurn(aimHeading, getGunHeadingRadians());
			
			setTurnGunRightRadians(gunTurn);
			
			boolean haveAShot = aiming.haveShot(gunTurn, myLocation, getBattleFieldWidth(), getBattleFieldHeight());
			if(haveAShot) {
				setFire(3);
			}
		}

	}
	
	public void onSkippedTurn() {
		out.println("Missed Turn");
	}
}
