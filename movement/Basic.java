package movement;

import coordinates.*;

public class Basic implements Movement{
	
	@Override
	public double getTurn(double heading, double enemyBearing) {
		double idealHeading1 = (Math.PI / 2) + enemyBearing;
		double idealHeading2 = (3 * Math.PI / 2) + enemyBearing;
		
		double turn1 = idealHeading1 - heading;
		double turn2 = idealHeading2 - heading;
		
		double turn = Math.min(turn1, turn2);
		
		if (Math.PI > Math.abs(turn)) {
			turn = turn - Math.PI;
		} 

		return turn;
	}
	
	public CompassPolarVector getMovement(CartesianVector myLocation, CartesianVector newLocation){
		CartesianVector pathToMove = myLocation.subtract(newLocation);
		
		return new CompassPolarVector(pathToMove);
	}
	
	

}
