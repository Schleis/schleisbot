package movement;

public interface Movement {
	public double getTurn(double heading, double enemyBearing);
}
