package aiming;

import numbers.RoundedDouble;
import coordinates.*;
import robocode.Rules;
import schleisBot.EnemyBot;

public class offsetAim implements AimStrategy{
	CompassPolarVector enemyVelocityVector;
	CompassPolarVector enemyLocationVector;
	CompassPolarVector enemyHitLocation;
	
	public offsetAim(EnemyBot e) {
		setEnemyVectors(e);
	}
	
	public double getGunHeading(double BulletPower) {
		
		if (0 == enemyVelocityVector.getMagnitude() || enemyMovingAlongBearing()) {
			enemyHitLocation = enemyLocationVector;
			return enemyLocationVector.getTheta();
		}
		
		CartesianVector enemyLocation = enemyLocationVector.toCartesian();
		CartesianVector enemyVelocity = enemyVelocityVector.toCartesian();
		
		double bulletVelocity = 20 - 3 * BulletPower;
		
		double timeToTarget = timeToTarget(enemyLocation, enemyVelocity, bulletVelocity);

		CartesianVector enemyDistanceTraveled = enemyVelocity.scale(timeToTarget);
		
		CartesianVector enemyFutureLocation = enemyLocation.add(enemyDistanceTraveled);
	
		enemyHitLocation = new CompassPolarVector(enemyFutureLocation);
		
		return enemyHitLocation.getTheta();
	}

	public boolean haveShot(double gunTurn, CartesianVector myBotLocation, double arenaWidth, double arenaHeight) {
		CartesianVector arenaLocationOfHit = myBotLocation.add(enemyHitLocation.toCartesian());
		
		if (0 > arenaLocationOfHit.getX() || 0 > arenaLocationOfHit.getY() || arenaWidth < arenaLocationOfHit.getX() || arenaHeight < arenaLocationOfHit.getY()) {
			return false;
		}
		
		if (0.05 > Math.abs(gunTurn)){
			return true;
		}
		
		return false;
	}
	
	public double getGunTurn(double aimHeading, double gunHeading) {
		double gunTurn = aimHeading - gunHeading;
		
		gunTurn = Math.min(gunTurn, Rules.GUN_TURN_RATE_RADIANS);
		
		return gunTurn;
	}
	private void setEnemyVectors(EnemyBot e) {
		enemyVelocityVector = new CompassPolarVector(e.getHeading(), e.getVelocity());
		enemyLocationVector = new CompassPolarVector(e.getBearing(), e.getDistance());
	}
	
	private double timeToTarget (CartesianVector location, CartesianVector velocity, double BulletSpeed) {
		double locationX = location.getX();
		double locationY = location.getY();

		double velocityX = velocity.getX();
		double velocityY = velocity.getY();

		double a = Math.pow(velocityX, 2) + Math.pow(velocityY, 2) - Math.pow(BulletSpeed, 2);
		double b = 2*locationX * velocityX + 2*locationY * velocityY;
		double c = Math.pow(locationX, 2) + Math.pow(locationY, 2);
		
		double root = (b * b) - 4 * a * c;

		if (0 > root) {
			return 0.00; //Really should throw an exception here
		}
		
		double answer1 = (-b + Math.sqrt(root)) / (2 * a);
		double answer2 = (-b - Math.sqrt(root)) / (2 * a);

		//Return the time that is positive
		if (0 > answer1) {
			return answer2;
		} else {
			return answer1;
		}
	}
	
	private boolean enemyMovingAlongBearing() {
		double differenceHeadingAndBearing = enemyVelocityVector.getTheta() - enemyLocationVector.getTheta();
		
		differenceHeadingAndBearing = RoundedDouble.round(differenceHeadingAndBearing, 6);
		
		if(0 == differenceHeadingAndBearing || RoundedDouble.round(Math.PI,6) == differenceHeadingAndBearing){
			return true;
		} else {
			return false;
		}
	}
	
}
