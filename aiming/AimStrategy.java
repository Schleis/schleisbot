package aiming;

import coordinates.*;

public interface AimStrategy {
	public double getGunHeading(double BulletPower);
	public double getGunTurn(double aimHeading, double gunHeading);
	public boolean haveShot(double gunTurn, CartesianVector myBotLocation, double arenaWidth, double arenaHeight);
}
