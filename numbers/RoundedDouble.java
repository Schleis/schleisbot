package numbers;

public class RoundedDouble{
	
	static public double round(double n, int places) {
		int magnitude = (int) Math.pow(10, places);
		long digits = Math.round(n * magnitude);
		
		return (double)digits/magnitude;
	}
}
